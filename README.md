Introduction
============

CrFinder searches a given directory containing camera raw files for pictures of a color reference such as the X-Rite Color Checker Passpoert. It then prepares the images so so argyll can create a color profile from that image and invokes argyll to create color profiles. 

Currently supported color references include the X-Rite Color Checker Passport Photo and similar color references. However, the script was designed to allow the use of other types of references too, so it shouldn't be to difficult to adapt it to other types of color reference.

The script is reasonably fast and detection is reliable. On my laptop (6-core Coffelake) it processes approx 30 images per minute (Images taken with a Sony Alpha 7R2 at full resolution, 6% of the images contain a color reference). Among 97 images of color references tested only one was not correctly identified. In the image where the color reference was not detected the color reference was too small to be detected.

Installation
============

You need argyll cms installed. On Debian based distros:

sudo apt install argyll

Install crfinder via pip:

pip3 install CrFinder

Usage
=====

crfinder [directory]

[directory] contains the raw images to search for color references.

Principle of Operation
======================

Assumptions
-----------

This script relies on the following properties of the Color Checker (and similar color references):
* Patches are surrounded by a balck or white background: This allows to get a black and white image with nicely delineated patches by appying a simple threshold.
* Patches are uniform and have no pattering in them.
* Patches are squares. All other shapes can be ignored.
* Patches are on a rectangular grid. Each patch has a neighbour within a certain distance. Further, the position of patches with insufficient contrast to the background can be inferred from the position of other patches.