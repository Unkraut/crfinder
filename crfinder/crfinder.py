#!/usr/bin/python3

# -*- coding: utf-8 -*-

# Copyright 2018, Simon Waid
# This file is part of CrFinder.
#
#    CrFinder is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    CrFinder is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von CrFinder.
#
#    CrFinder ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
#    veröffentlichten Version, weiter verteilen und/oder modifizieren.
#
#    CrFinder wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.



import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))


import cv2 as cv
# import the necessary packages
#from pyimagesearch.shapedetector import ShapeDetector

from multiprocessing import Pool, cpu_count
import rawkit # rawkit.options import WhiteBalance, colorspaces, gamma_curves, 
from rawkit.util import discover
from crfinder.patches import findCandiatePatches, mergeByNeighborhood
from crfinder.image import Image
from crfinder.colorRef import ColourReference
from crfinder.config import Config
from crfinder.argyll import Argyll
import numpy as np
from subprocess import call
#print(rawkit.VERSION)
import gc
#import objgraph, copy

DIR_FAILED='noRefDetected'
CONFIG='../argyllSettings/colorCheckerPassport.ini'
DIR_REF='refDetected'
DEBUG=False
PARALLEL= True #not DEBUG

#TODO: Perfom trapezoid transform instead of simple rotation.

class FileHandler():
    
    def __init__(self):
        pass
        
class ProcessingHelper():
    
    def __init__(self):
        self.f=[]
        
    def add(self, function, param):
        '''
        :param function:
        :param param:
        '''
        if not type(param) == list:
            raise(RuntimeError('Want a parameter list'))
        self.f.append({'function': function, 'param': param} )
        
    def execute(self, parallel):
  
        if parallel:
            p=Pool(int(cpu_count()-1))
            p.map(self._exec, self.f) 
        else:
            for p in self.f:
                self._exec(p)
        
    def _exec(self, param):
        
        param['function'](*param['param'])
        # Free some memory
        gc.collect()
        #objgraph.show_most_common_types(limit=20)

def main():
    
    print('This program comes with ABSOLUTELY NO WARRANTY;\nThis is free software, and you are welcome to redistribute it under certain conditions; See LICENSE.txt for details. \n')
    
    
    n=1
    if len(sys.argv) < 2:
        print('Usage: cfinder [directory] \n\n[directory] contains the raw images to search for color references.\n' )
        quit()
        
    
    if os.path.isdir(sys.argv[n]):
        directory= sys.argv[n]
    else:
        print('{} is not a directory'.format(sys.argv[n]))
    
    
    # Rawkit discover is not working with german accents to let's use#something else.
    fileList=os.scandir(directory)
    
    p=ProcessingHelper()
    for file in fileList:
        # Interpret file name as utf-8. Might not work on M$ Windows.
        file=file.path
        directory=os.path.dirname(file)
        base, ext=os.path.splitext(file)
        if ext=='.ARW':
            p.add(doMagic, [file, directory])
    
    p.execute(PARALLEL)
        
def doMagic(fileName, dirOutput):
    '''
    Checks if the file given by fFalseileName is a color reference. The result is written to the directory dirOutput
    
    :param fileName:
    :param dirOutput:
    '''
    
    # Load configuration
    config=Config(CONFIG)
    # Load the image into memory
    image=Image(fileName, widthSmall=config.processingWidth)
    
    # Create directories so we can store debugging info. 
    # Try to create the output directory tree
    dirFailed=os.path.join(dirOutput, DIR_FAILED)
    dirRef=os.path.join(dirOutput, DIR_REF)
    # Strip directory and extension.
    fileNameBase=os.path.splitext(os.path.basename(fileName))[0]
    try:
        os.mkdir(dirOutput)
    except:
        pass
    try:
        os.mkdir(dirFailed)
    except:
        pass
    try:
        os.mkdir(dirRef)
    except:
        pass
   
    # Compose file names for thumbnails.
    fnameThumbFailed=os.path.join(dirFailed, fileNameBase+ '.jpg')
    fnameThumbPassed=os.path.join(dirRef, fileNameBase+ '.jpg')
   
    # If thumbnails exist, don't process the file.
    if os.path.isfile(fnameThumbFailed) or os.path.isfile(fnameThumbPassed):
        return
   
    # Get the image in format suitable for processing with opencv
    imgBrg=image.getImage(8, 'srgb_brg', True)
    # If loading the file fails, return.
    
    if imgBrg is None:
        print('Failed to load into memory: {}. Skipping'.format(fileName))
        return
    print('Working on file: {}. Output directory: {}'.format(fileName, dirOutput))
    
    for thr in config.thresholds:
        # Useful values are 80 and 160
        print('Threshold', thr)
        colorRefsOnGrid=detectColorChecker(image, config, thr, DEBUG)
        if colorRefsOnGrid != []:
            print('Treshold leading to detection: {}'.format(thr))
            break
            
    # Store a thumbnail of thnodee image so we can benchmark this tool.
    print('{} color references are on grid'.format(len(colorRefsOnGrid)))

    # Save the image of the transformed, but otherwise unmodified reference
    for i, colorRef in enumerate(colorRefsOnGrid):
        fname=os.path.join(dirRef, fileNameBase+'_transf_{}.tif'.format(i))
        imageTransformed= colorRef.imageTransformed(16, 'raw_brg', False, fname)
        
    # 4. Perform white balance using the brightest patch and detect the color of all patches
    for i, colorRef in enumerate(colorRefsOnGrid):
        fname=os.path.join(dirRef, fileNameBase+'_forArgyll_{}.tif'.format(i))
                    
        # 6. Adapt the brighness and black point to match the expected values
        # Note: At the moment this is not effective!
        # 7. Store the image for processing by argyll.
        targetBright=np.mean([88.15853, 91.54682, 73.20712])/np.mean([96.4200, 100.000, 82.4910])
        targetDark=np.mean( [2.845977, 2.957510, 2.601504])/np.mean([96.4200, 100.000, 82.4910])
        
        colorRef.writeAdjustedImage(fname, targetDark, targetBright, debug=DEBUG)
        # 
        argyll=Argyll(config)
        cmd=argyll.getScaninCmd(colorRef, fname)
        
        print(cmd) 
        call(cmd)
        argyll.addBlackWhiteToTi3()
        argyll.executeColprof()
        
        # 5. Compare the color to the reference and verify this is not a false positive.
        # TODO: implement this
        
        # Store the imge and the found patches for debugging and evaluation purposes.
        # 7. Store the image for processing by argyll.
        fname='test/test{}.tif'.format(i)
        if imageTransformed is None:
            print('Failed!')
        else:
            pass
    
    if True:
        if len(colorRefsOnGrid) < 1:
            image.saveThumb(fnameThumbFailed)
        else:
            image.saveThumb(fnameThumbPassed)
   

def detectColorChecker(image, config, threshold, debug):
    
    imgBrg=image.getImage(8, 'srgb_brg', True)

    # Find candidate patches in the image
    candidates=findCandiatePatches(imgBrg, config, threshold, debug=debug)
    
    # 1. Filter the found patches based on the knwoledge we have on the geometry our color refence.
    # 2. Merge patches to potential color references.
    colorRefs=mergeByNeighborhood(imgBrg, candidates, config, debug=debug)
    
    # 3. Find patches not detected by findCandiatePatches based on our knowledge on the 
    # color reference geometry. 
    colorRefsOnGrid=[]
    for i, ref in enumerate(colorRefs):
        # Create a ColorReference object. 
        print('Color Reference has {} patches'.format(len(ref)) )
        colorRef=ColourReference(image, ref, config)
        if debug:
            colorRef.showNeighbourhood()
    
        # Find patches having insufficient contrast to the background,
        # If finding patches was unsuccesfull continue with next candidate
        if not colorRef.findRemainingPatches(debug):
        #if True:
            print('Patches are not on a grid for potential color reference no. {}. Ignoring this candidate'.format(i)) 
        else:
            print('Patches are on a grid for potential color reference no. {}'.format(i))
            colorRefsOnGrid.append(colorRef)
    return colorRefsOnGrid

if __name__ == '__main__':
    
    #print(cv.__version__)y
    main()
    
    