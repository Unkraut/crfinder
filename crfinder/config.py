# -*- coding: utf-8 -*-


# Copyright 2018, Simon Waid
# This file is part of CrFinder.
#
#    CrFinder is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    CrFinder is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von CrFinder.
#
#    CrFinder ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
#    veröffentlichten Version, weiter verteilen und/oder modifizieren.
#
#    CrFinder wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

from configparser import ConfigParser
import math
class Config():
    def __init__(self, cfgFile):
        self.parser=ConfigParser()
        self.parser.read(cfgFile)
        
        self.processingWidth=self._getValue('Image', 'ProcessingWidth')
        self.thresholds=self._getValue('Image', 'Thresholds')
        self.radiusAvgColor=self._getValue('Image', 'RadiusAvgColor')
        self.limitLow=self._getValue('PatchDetection', 'limitLow')
        self.limitHigh=self._getValue('PatchDetection', 'limitHigh')
        self.threshold=self._getValue('PatchDetection', 'threshold')
        self.NoBins=self._getValue('PatchDetection', 'NoBins')
        self.angleTolerance=self._getValue('PatchDetection', 'AngleTolerance')/180*math.pi
        self.toleranceSize=self._getValue('PatchDetection', 'toleranceSize')
        
        self.maxNeighbourDist=self._getValue('ColorReference', 'MaxNeighbourDist')
        self.toleranceGrid=self._getValue('ColorReference', 'ToleranceGrid')
        self.noPatchesX=self._getValue('ColorReference', 'NoPatchesX')
        self.noPatchesY=self._getValue('ColorReference', 'NoPatchesY')
        self.minPatches=self._getValue('ColorReference', 'MinPatches')
        self.maxPatches=self._getValue('ColorReference', 'MaxPatches')
        self.fiducialLocation=self._getValue('Argyll', 'fiducialLocation')
        self.fileCht=self._getValue('Argyll', 'fileCht')
        self.fileCie=self._getValue('Argyll', 'fileCie')
        
    def _getValue(self, section, entry):

        value=self.parser.get(section, entry)
        value=eval(value)
        
        return value
