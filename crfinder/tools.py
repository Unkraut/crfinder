# -*- coding: utf-8 -*-


# Copyright 2018, Simon Waid
# This file is part of CrFinder.
#
#    CrFinder is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    CrFinder is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von CrFinder.
#
#    CrFinder ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
#    veröffentlichten Version, weiter verteilen und/oder modifizieren.
#
#    CrFinder wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.


import numpy as np
import cv2 as cv
import cv2
from matplotlib import pyplot as plt
import tifffile as tiff
import math
# import the necessary packages
#from pyimagesearch.shapedetector import ShapeDetector
import argparse
import ctypes
import colormath
import imutils
import igraph
import copy

import numpy as np
from matplotlib import pyplot as plt

from rawkit.raw import Raw
from rawkit.options import WhiteBalance
from rawkit.options import Options as RawOptions
from rawkit.options import colorspaces, gamma_curves
import rawkit # rawkit.options import WhiteBalance, colorspaces, gamma_curves, 
from scipy.optimize import leastsq
    
    
def showImage(image):
    '''
    Resizes the image and displays it
    '''
    DEBUGSIZE=1000
    shape=image.shape
    if shape[0] > shape[1]:
        img = imutils.resize(image, height=DEBUGSIZE)
    else:
        img = imutils.resize(image, width=DEBUGSIZE)
    
    cv2.imshow("Image", img)
    cv2.waitKey(0)
    
def distance(p0, p1):
    return math.sqrt((float(p0[0]) - float(p1[0]))**2 + (float(p0[1]) - float(p1[1]))**2)
    
def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy

def calcAngle(p1, p2):
    ang1 = np.arctan2(*p1[::-1])
    ang2 = np.arctan2(*p2[::-1])
    return ang1 - ang2

def calcAngle2(p1, p2):
    p2=np.subtract(p2,p1)
    #ang1 = np.arctan2(*p1[::-1])
    ang = np.arctan2(*p2[::-1])
    return ang
