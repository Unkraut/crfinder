# -*- coding: utf-8 -*-

# Copyright 2018, Simon Waid
# This file is part of CrFinder.
#
#    CrFinder is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    CrFinder is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von CrFinder.
#
#    CrFinder ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
#    veröffentlichten Version, weiter verteilen und/oder modifizieren.
#
#    CrFinder wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.


import cv2
import imutils
import copy

import numpy as np

from rawkit.raw import Raw
from rawkit.options import colorspaces, gamma_curves
import rawkit # rawkit.options import WhiteBalance, colorspaces, gamma_curves, 
from rawkit.options import WhiteBalance

class Darktable():
    
    def __init__(self, fileName, config):
        self.fileName=fileName
        self.config=config
        
    def processImage(self):
        pass
        

class Image():
    def __init__(self, filename, widthSmall=None):
        '''
        :param filename: File to load
        :param width: Width of the image in memory. The loaded image is rescaled to this width.
        
        :ivar ratio: ratio between original image and :py:var:`imgSmall`
        '''
        self.filename=filename
       
        self.widthSmall=widthSmall
        self.imgSmallBrg=None
        self.cache={}        
        self.greybox=None
        
    def saveThumb(self, fileName):
        '''
        Saves the image at its small size , 8 bit, 
        
        '''
        image=self.getImage(8, 'srgb_brg', True)
        cv2.imwrite(fileName, image)


    def getImage(self, bps=8, color='default', small=True):
        '''
        Retruns the image as numpy array. Requests are cached, therefore, please don't alter the returned image. Feel free to call this often. 
        
        :param bps: 
        :param colors:
            * default will use the default color space/gammut setting of rawkit.
            * brg behaves as default, but will return an image in the brg format instead of rgb
            * xyz will return an image in the xyz color space with linear gammut
        :param small: If tru, the image will be rescaled to the width given by widthSmall.
        
        :return None if loading the image fails. Otherwise, a numpy array is returned.
        '''
#        :param width: A positive number will scale the image to the requested width. A negative number returns the image at its original size.
        
        key=str(bps)+str(color)+ str(small)
        if key in self.cache:
            return self.cache[key]
    
        if not bps==8 and not bps==16:
            raise(RuntimeError('Invalid bps {}'.format(bps)))
        try:
            raw= Raw(filename=self.filename)
        except:
            print(self.filename)
            raise
            return None    
        #raw =self.raw
        
#        raw=copy.copy(self.raw)
        raw.options.bps=bps
        if color == 'default':
            raw.options.colorspace = colorspaces.srgb
            raw.options.gamma = gamma_curves.srgb
            brg=False
        elif color == 'srgb_brg':
            raw.options.colorspace = colorspaces.srgb
            raw.options.gamma = gamma_curves.srgb
            brg=True
        elif color == 'raw_brg':
            raw.options.auto_brightness=False
            raw.options.brightness=2
            raw.options.colorspace = colorspaces.raw
            raw.options.gamma = gamma_curves.linear
            brg=True
        
        elif color == 'xyz':
            brg=False
            raw.options.auto_brightness=False
            #raw.options.brightness=1.1
            #raw.options.darkness=700
            raw.options.colorspace = colorspaces.xyz
            raw.options.gamma = gamma_curves.linear
        else:
            raise(RuntimeError('Invalid value for color supplied. {}' .format(color)))
        raw=self._wb(raw)
        buffer=raw.to_buffer()
        if raw.options.bps==8:
            img=np.frombuffer(buffer, dtype=np.uint8)
        elif raw.options.bps==16:
            img=np.frombuffer(buffer, dtype=np.uint16)
        if raw.metadata.orientation ==1:
            newshape=[raw.metadata.height,raw.metadata.width, 3]
        else:
            newshape=[raw.metadata.width,raw.metadata.height, 3]
            
        img=np.reshape(img, newshape)
        
        if small:
            width=img.shape[1]
            height=img.shape[0]
            if height > width:
                img = imutils.resize(img, height=self.widthSmall)
                self.ratio=height/self.widthSmall
            else:
                img = imutils.resize(img, width=self.widthSmall)
                self.ratio=width/self.widthSmall
        
        if brg:
            img=cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

        self.cache[key]=img
        # Close thw raw file otherwise we run out of memory
        raw.close()
        
        return img
    
    def detectColor(self, radius, cog, bps, color, small, debug=False, image=None):
        '''
        
        :param bps: directy formwarded to :py:meth:`.getImage`. See :py:meth:`.getImage` for documentation. 
        :param color: directy formwarded to :py:meth:`.getImage`. See :py:meth:`.getImage` for documentation. 
        :param small: directy formwarded to :py:meth:`.getImage`. See :py:meth:`.getImage` for documentation. 
        '''
        #DEBUG=False
        if image is None:
            image=self.getImage(bps, color, small)
        # Create a binary image as mask
        mask = np.ones((image.shape[0], image.shape[1], 3), dtype = "uint8")
        # Draw a circle at the center of the patch on the mask 
        
        cv2.circle(mask, tuple(np.array(cog, dtype=np.int)),  int(radius), (0, 0, 0), thickness=-1)
        if debug:
            #bg=copy.deepcopy(image)
            dbg=np.array(image, dtype=np.uint16)
            if bps == 16:
                cv2.circle(dbg, tuple(np.array(cog, dtype=np.int)),  int(radius), (100*255, 100*255, 255*255), thickness=-1)
            else:
                cv2.circle(dbg, tuple(np.array(cog, dtype=np.int)),  int(radius), (100, 100, 255), thickness=-1)
            cv2.imshow("Image", dbg)
            cv2.waitKey(0)
        # Apply the mask to the image
        imgMasked=np.ma.array(image, mask=mask)
        if debug:
            dbg=np.array(imgMasked, dtype=np.uint16)

            cv2.imshow("Image", np.ma.filled(dbg, 0))
            cv2.waitKey(0)
        
        # Get average color
        color=imgMasked.mean(axis=(0, 1))
        return color.filled() 
    
    def drawCircles(self, img, radius, patches, color, useCog=False, small=True):    
        dbg=copy.copy(img)
        if not small:
            radius*=self.ratio
        for patch in patches:    
            if useCog:
                loc=np.array(patch.cog, dtype=np.int)
            else:
                loc=np.array(patch.cogNode, dtype=np.int)
            if not small:
                loc=(loc.astype(np.float)*self.ratio).astype(np.int)   
            #print(dbg.shape, loc, radius)
            if not patch.color is None:
                cv2.circle(dbg, tuple(loc),  patch.radius, patch.color, thickness=-1)
            cv2.circle(dbg, tuple(loc),  patch.radius, color, thickness=2)
        return dbg
        #cv2.imshow("Image", dbg)
        #cv2.waitKey(0)
    
    def getTransformedImage(self, size, m, bps, color, small):
        '''
        
        :param size: Size of the target image. If small is False, the size will be scaled. Therefore, please provide a soze that matches the small image. 
        :param m: Transformation matrix. The matrix is not scaled. Therefore, please provide a matrix corresponding to the requested size of the image.
        :param bps: directy formwarded to :py:meth:`.getImage`. See :py:meth:`.getImage` for documentation. 
        :param color: directy formwarded to :py:meth:`.getImage`. See :py:meth:`.getImage` for documentation. 
        :param small: directy formwarded to :py:meth:`.getImage`. See :py:meth:`.getImage` for documentation. 
        
        '''
        
        image=self.getImage( bps, color, small)
        if not small:
            size=tuple(np.array((np.array(size, dtype=np.float)*self.ratio), dtype=np.int))
        imgTrans=cv2.warpPerspective(image, m, size)
        return imgTrans

    def performWhiteBalance(self, cog, size, small=True):
        '''
        Performs a white balange using the data in the square around cog having the sidelenght size. 
        
        :param cog:
        :param size
        :param small: If true, coordinates refer to the small image. IF false, coordinates refer to the full image.
        '''
        DEBUG=False
        if DEBUG:
            img=self.getImage(8, 'brg', True)
            dbg=copy.deepcopy(img)
            cv2.circle(dbg, tuple(cog),  int(size[0]), (255, 255, 255), thickness=2)
            cv2.imshow("Image", dbg)
            cv2.waitKey(0)
        
        if small:
            cog=np.multiply(cog.astype(np.float),self.ratio).astype(np.int)
            size=np.multiply(size.astype(np.float),self.ratio).astype(np.int)
    
        # After changing the white balance, the cache becomes invalid. Therefore clear it.
        self.cache={}
        if DEBUG:
            print(cog, size)
        greybox=[cog[0]-size[0]/2, cog[1]-size[1]/2, cog[0]+size[0]/2, cog[1]+size[1]/2]
        self.greybox=np.array(greybox, dtype=np.int)
    
    
    def _wb(self, raw):
        if not self.greybox is None:
            raw.options.white_balance = WhiteBalance(greybox=self.greybox)
        return raw