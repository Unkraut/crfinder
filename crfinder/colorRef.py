# -*- coding: utf-8 -*-


# Copyright 2018, Simon Waid
# This file is part of CrFinder.
#
#    CrFinder is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    CrFinder is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von CrFinder.
#
#    CrFinder ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
#    veröffentlichten Version, weiter verteilen und/oder modifizieren.
#
#    CrFinder wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.


import numpy as np
from crfinder.tools import rotate, distance, calcAngle, calcAngle2, showImage
import cv2
import math
import copy
from scipy.optimize import leastsq
from colormath.color_conversions  import convert_color
from colormath.color_objects import XYZColor, LabColor
#from networkx.classes.function import neighbors
import tifffile


class ColourReferenceElement():
    DEBUG=False
    def __init__(self, cog, area, config):
        '''
        
        :ivar cog:
        :ivar area:
        :ivar neighbours:
        :ivar sideLenght:
        :ivar cogNode:
        :ivar cogRot: Center of gravity after roation (set by :py:class:`ColourReference.rotatePatches`)
        :ivar color:
        :ivar config:
        :ivar loc: [row, column] 
        :vartype loc: list of int
        :ivar neighboursSorted: [East, NortEast, North, Northwest, West, SouthWest, South, Southeast]
        '''
        self.cog=np.array(cog)
        self.area=area
        self.neighbours=[]
        self.sideLenght=math.sqrt(self.area)
        self.cogRot=None
        self.color=None
        self.config=config
        self.initCogNode()
        self.initLoc()
        self.initNeighSorted()
        
        
    def initCogNode(self):
        self.cogNode=None

    def initLoc(self):
        self.loc=[None, None]
        
    def initNeighSorted(self):
        self.neighboursSorted=[None for i in range(8)]
        
        
    def addNeighbour(self, neighbour):
        '''
        Adds a neighbour. Duplicate additions are ignored.
        
        '''
        if not neighbour in self.neighbours and not neighbour == self:
            self.neighbours.append(neighbour)
    
    @property
    def radius(self):
        radiusRel=self.config.radiusAvgColor
        radius=int(math.sqrt(self.area)*0.5 *radiusRel)
        return radius

class ColourReference():
    DEBUG=False
    def __init__(self, image, patchList, config):
        '''
        :param patchList: List of instances of :py:class:`ColorReferenceElement`
        
        
        :ivar patches:
        :ivar hiddenPatches:
        :ivar mForward:
        :ivar mForwardLarge: 
        
        '''
        self.patches=patchList
        self.image = image
        self.config=config
        self.mForward=None
        self.mForwardLarge=None
        self.rotated=False
                
        # TODO: Fix this inconsistency somewhere else!!!!
        # This is ugly!
        patchesInclNeighb=set()
        
        for i in range(config.noPatchesX+ config.noPatchesX):
            for patch in self.patches:
                for neighbour in patch.neighbours:
                    patchesInclNeighb.add(neighbour)
        
            self.patches=list(patchesInclNeighb)
        
        if set(self.patches) != patchesInclNeighb:
            print( len(self.patches), len(patchesInclNeighb))
            self.showPatches(list(patchesInclNeighb))
            raise(RuntimeError('Neighbours incosnistent'))
       
    def showPatches(self, patches=None):
        #
        if patches is None:
            patches=self.patches
        imgTrans=self.image.getImage(8, 'srgb_brg', True)
        img= self.image.drawCircles(imgTrans, self.config.radiusAvgColor, patches, (255, 255, 255), useCog=True, small=True)    
        showImage(img)
        
    def showNeighbourhood(self, patches=None):
        #
        if patches is None:
            patches=self.patches
        imgTrans=self.image.getImage(8, 'srgb_brg', True)
        img= self.image.drawCircles(imgTrans, self.config.radiusAvgColor, patches, (255, 255, 255), useCog=True, small=True)    
        for patch in patches:
            for neighbour in patch.neighbours:
                cv2.line(img, tuple(patch.cog), tuple(neighbour.cog), (255,100,100), 1)
                cirCog=np.array((patch.cog + neighbour.cog)/2, dtype=np.int)
                cv2.circle(img, tuple(cirCog), 3,  (100,255,100), 1)
            print(patch.sideLenght)
        showImage(img)
    
    @property
    def center(self):
        cogsReal=np.array([patch.cog for patch in self.patches], np.float32)
        center=np.mean(cogsReal, axis=0)
        return center
    
    def rotatePatches(self, center=None, angle=None):
        '''
        Rotates the patches around the given center. 
        The rotated cog is stored in the patches property cogRot.
        
        :param center: Center for rotation. If not given, the center is automatically detected.
        :param angle: anglge in radians. If not given, the angle is automatically detected.
        '''
        if self.rotated:
            return
        self.rotated=True
        
        if center is None:
            # Find center around which we rotate.
            center=self.center
            
        if angle is None:
            # The (initial guess) roation angle is determined as the angel between two arebitrary patches 
            
            cogA=self.patches[0].cog
            cogB=self.patches[0].neighbours[0].cog
        
            myAngle=calcAngle2(cogA, cogB)
            self._rotate(center, myAngle)
                
            # Find the angle for which the boxSize in vertical direction is minimal
            _, minSize=self.getBoundingBox()
            #for patch in self.patches:
            #    cogA=patch.cog
            #    for neighbour in self.neighbours:
            #        cogB=neighbour.cog
            #        angle=calcAngle2(cogA, cogB)
            for a in range(100):
                angle=2*math.pi*a/100
                self._rotate(center, angle)
                _, size=self.getBoundingBox()
                if size[1] <= minSize[1]:
                    myAngle=angle
                    minSize =size
                 
        print('Angle', np.array(angle)/math.pi*180)
        
        self._rotate(center, myAngle)
        
        # Get a bounding box and check orientation if it's not landscape rotate for another 90 degree.    
        box, boxSize=self.getBoundingBox()
        # If the rotation is not alredy langscape rotate.
        if  boxSize[1] > boxSize[0]:
            angle += np.pi/2
            self._rotate(center, myAngle)
        
        # Store the angle  We need it later on.
        self.angle=myAngle
   
    def _rotate(self, center, angle):
        if self.DEBUG:
            for patch in self.patches:
                print(patch.cog)
        
        for patch in self.patches:
            if np.all(patch.cog != center): 
                patch.cogRot=rotate(center, patch.cog, angle)
            else:
                patch.cogRot=patch.cog
            #TODO: This should not be necessary !!!!!
            #TODO: Fix the bug leading to neigbours not being inclued in patches.
            for neighbour in patch.neighbours:
                if neighbour.cogRot is None:
                    if np.all(neighbour.cog != center): 
                        neighbour.cogRot=rotate(center, neighbour.cog, angle)
                    else:
                        neighbour.cogRot=neighbour.cog
            
        for patch in self.patches:
            if patch.cogRot is None:
                raise(RuntimeError('Bug!'))

        for patch in self.patches:
            for neighbour in patch.neighbours:
                if neighbour.cogRot is None:
                    raise(RuntimeError('Bug!'))
    
    def getBoundingBox(self):
        '''
        Finds a bounding box for the center of gravity of all patches.
        
        Note 1: the patches propert cogRot will be used for the box detection. Will fail if not present.
        Note 2: hidden patches are not included
        
        :returns: [cornerLeftLower, cornerRightUpper], boxSize
        '''
        DEBUG=False
        # Find a bounding box. 
        cornerLeftLower=self.patches[0].cogRot
        cornerRightUpper=self.patches[0].cogRot
        for patch in self.patches:
            if DEBUG:
                print(patch.cog, patch.cogRot)
            cornerLeftLower=np.min([cornerLeftLower, patch.cogRot], axis=0)
            cornerRightUpper=np.max([cornerRightUpper, patch.cogRot], axis=0)
        
        boxSize=cornerRightUpper-cornerLeftLower
        return [cornerLeftLower, cornerRightUpper], boxSize
    
    def _calcMeanPatchArea(self):
        '''
        Returns the average area of the patches
        '''
        areas=[patch.area for patch in self.patches]
        self.areaAvg=np.mean(areas)
        
        return self.areaAvg
    
    def getEdges(self):
        '''
        Returns a list of edge patches. The order is as follows: 
        Max x, Max y, Min x, Min y
        
        :returns: List of instaces of :py:class:`.ColorReferenceElement`
        '''
        #DEBUG=False
        
        # Initialize the lost of edge patches
        edge=[]
        for i in range(4):
            edge.append(self.patches[0])
                    
        for patch in self.patches:
            if patch.cogRot[0]< edge[0].cogRot[0]:
                edge[0]=patch
            if patch.cogRot[1]< edge[1].cogRot[1]:
                edge[1]=patch
            if patch.cogRot[0]> edge[2].cogRot[0]:
                edge[2]=patch
            if patch.cogRot[1]> edge[3].cogRot[1]:
                edge[3]=patch
                
        return edge
    
    def getCorners(self):
        '''
        Searches for the 4 patches closest to the edges of the bounding box.
        
        '''
        
        bbox, _=self.getBoundingBox()
        #  The edges are all permutations ofteh bounding box coordinates.
        coord=[bbox[0], bbox[1], [bbox[0][0], bbox[1][1]], [bbox[1][0], bbox[0][1]]]
        
        edges=[]
        for c in coord: 
            # Initialize distance
            dist=distance(c, self.patches[0].cogRot)
            # Loop over all patches and find the closest to the given coordinates.
            for patch in self.patches:
                d =distance(c, patch.cogRot)
                # Use smaller equal so we also assign if patch 0 is the correct one
                if d <= dist:
                    dist=d
                    thisEdge= patch
            edges.append(thisEdge)
        
        return edges
        
    def calcPerspectiveTransform(self, ratio, direction='forward'):
    
        # Get the edge patches to calcualte an initial guess
        edges=self.getCorners()
        # Get the center of gravities of edge patches and scale
        cogsNodeEdge=np.array([patch.cogNode for patch in edges], np.float32)*ratio
        cogsRealEdge=np.array([patch.cog for patch in edges], np.float32)*ratio
        
        if direction == 'forward':
            mInitialGuess = cv2.getPerspectiveTransform(cogsNodeEdge, cogsRealEdge)
        elif direction == 'backward':
            mInitialGuess = cv2.getPerspectiveTransform(cogsRealEdge, cogsNodeEdge)
        else:
            raise RuntimeError('Invalid direction')

        cogsNode=np.array([patch.cogNode for patch in self.patches], np.float32)*ratio
        cogsReal=np.array([patch.cog for patch in self.patches], np.float32)*ratio
        
        # Start with a perspective transform
        # TODO: Not a good guess, improve!
        if direction == 'forward':
            cogsReal=np.array([cogsReal])
            errInitial=self._errorPerspectiveTransform(mInitialGuess, cogsReal, cogsNode)
            mFit, _=leastsq(self._errorPerspectiveTransform, mInitialGuess, args=(cogsReal, cogsNode))
            errFinal=self._errorPerspectiveTransform(mFit, cogsReal, cogsNode)   
        elif direction == 'backward':
            cogsNode=np.array([cogsNode])
            errInitial=self._errorPerspectiveTransform(mInitialGuess, cogsNode, cogsReal)
            mFit, _=leastsq(self._errorPerspectiveTransform, mInitialGuess, args=(cogsNode, cogsReal))
            errFinal=self._errorPerspectiveTransform(mFit, cogsNode, cogsReal)   
        else:
            raise(RuntimeError('Invalid direction'))
        
        # Fit the model to the location of the patches.
        errInitial=np.sqrt(np.mean(np.square(errInitial)))
        errFinal=np.sqrt(np.mean(np.square(errFinal)))
        
        print('Error before optimization: {}. After: {}'.format(errInitial, errFinal))
           
        mFinal=np.reshape(np.array(mFit, dtype=np.float32) , [3,3])
        
        return mFinal 
        
    def _errorPerspectiveTransform(self, H, cogsReal, cogsNode):
        '''
         Performes a perspecive transform on the cogsReal and calcuates the remaining error against cogsNode
    
        '''
        M = np.reshape(H, [3,3])
        M= np.array(M, dtype='float32')
        cogs=cv2.perspectiveTransform(cogsReal, M)[0]
        
        error=cogsNode-cogs
        return error.flatten()
    
    def _sortNeighbours(self, debug=False):
        '''
        
        :returns: False if sanity chek 2 or 3 described in _createGrid2 fails. Otherwise True.
        '''
        
        aTol= self.config.angleTolerance 

        for patch in self.patches:
            #print(patch.cogRot)
            for i, neighbour in enumerate(patch.neighbours):
                #print(i, patch.cogRot, neighbour.cogRot)
                #try:
                angle=calcAngle2(patch.cogRot, neighbour.cogRot)
                # We want angles from 0 to 2pi
                if debug:
                    print('Angle:',  angle/math.pi*180)
                if angle < 0:
                        angle=2*math.pi+angle
                #print(angle/math.pi*180)
                #print(i, neighbour.cogRot, 'Angle:',  angle/math.pi*180)
                # iterate over all carindal directions
                angleOk=False
                for j in range(9):
                    a=j*math.pi/4
                    #print(a+aTol, angle, a-aTol)
                    # Check id the angle is within the allowed angle for the given 
                    # The angle is always positive.
                    if (a+aTol > angle) and (a-aTol < angle):# or a-aTol+2*math.pi < angle):
                        # We handle the periodicity of the angle by checking both 0 and 360. They both have the same array index
                        if j == 8:
                            j=0
                        # If we were wihin tolerance, the angle was ok.
                        if debug:
                            print('Direction chosen is {}'.format(j))        
                        if patch.neighboursSorted[j] is None:
                            patch.neighboursSorted[j]=neighbour
                            angleOk=True
                            break       
                        else:
                            print('Failed . Reason: 1')      
                            return False
                if not angleOk:
                    print('Failed . Reason: 2')      
                    print('Angle not ok, ignoring')       
                    #return False

        return True
    
    def _createGrid2(self, debug=False):
        '''
        Creates a rectangular grid and assigns a grid index to all patches.
        
        Working principle: Patches are rotated so they approximately form a landscape oriented rectangle. 
        In this rectangle we can assign a row and colum number to each patch (from left to right and from top to bottom).
        We can assume that the leftmost patch is in the first colum and the topmost patch is in the first row. Similarly, 
        the righmost patch is in the last column and patch at the bottom is in the last row. 
        
        Starting from these patches we can can walk raound the neighbourhood and assign row and column numbers to the remaining patches.
        
        We don't know if the patches are really on a rectangular grid. Therefore, some sanity checks are performend. 
        These sanity checks are:
        
        1. Each patch must have a maximum of 4 neighbours. Note: This is always satiesfied if 2 and 3 are checked.
        2. The cardinal direction from one patch to the next must be identifiable. 
            We allow the angle between the patches to be in a window around the nominal value for each cardinal direction.
            If any angle is outsie the window we fail.
        3. Each patch must have a maximum of 1 neighbour in each cardinal direction.
        4. The number of rows and columns detected must correspond to the confiured value. 
        
        The following configuration values are employed:
        
        *
    
        
        '''
        # Check if the patches have already been rotated. If not rotate. Note: 
        # The check if rotation has taken place is perfomed by the rotation function.
        self.rotatePatches()
        # Find edge patches and assign row and column number.
        edges=self.getEdges()
        edges[0].loc[0]=0
        edges[1].loc[1]=0
        edges[2].loc[0]=self.config.noPatchesX-1
        edges[3].loc[1]=self.config.noPatchesY-1
        #aTol= self.config.angleTolerance 
        # Staring from each edge patch walk along the neighbourhood relations and visit all patches.
        if debug:
            imgTrans=self.image.getImage(8, 'srgb_brg', True)
            img=copy.copy(imgTrans)
            for patch in self.patches:
                center=np.array(patch.cogRot, dtype=np.int)
                cv2.circle(img, tuple(center), 5, (100,255,100), 2)
            for patch in  edges:
                center=np.array(patch.cogRot, dtype=np.int)
                cv2.circle(img, tuple(center), 5, (255,100,100), 2)
            showImage(img)
        
        if not self._sortNeighbours():
            print('Angles out of tolerance.')
            return False
        else:
            print('Angles, ok')
        if debug:
            print('Edges:')
            for patch in edges:
                print(patch.cogRot, patch.loc)
            print('Other patches:')
        
            for patch in self.patches:
                print(patch.cogRot, patch.loc)
        
        edges=set(edges)
        #TODO: we stay in this loop way too long. Stop when all is done:
        for k in range(len(self.patches)):
            for edge in list(edges):
                # Iterate over all neighbours
                for i, neighbour in enumerate(edge.neighboursSorted):
                    if not neighbour is None:
                        #print(edge.cogRot, neighbour.cogRot, i)
                        angle=np.pi/4*i
                        dy=int(np.clip(np.sin(angle)*10, -1, 1))
                        dx=int(np.clip(np.cos(angle)*10, -1, 1))
                        
                        if debug:
                            print(edge.cogRot, neighbour.cogRot, i, angle*180/math.pi,  'sign',  dx, dy,'index')
                        
                        if not edge.loc[0] is None:
                            if neighbour.loc[0] is None:
                                neighbour.loc[0] = edge.loc[0] + dx
                                edges.add(neighbour)
                            elif neighbour.loc[0] != edge.loc[0] + dx:
                                print('Failed.. indices inconsistent x', neighbour.loc, edge.loc, dx, dy, i)
                                return False
                        if not edge.loc[1] is None:
                            if neighbour.loc[1] is None:
                                neighbour.loc[1] = edge.loc[1] + dy
                                edges.add(neighbour)
                            elif neighbour.loc[1] != edge.loc[1] + dy:
                                print('Failed.. indices inconsistent y' , neighbour.loc, edge.loc, dx, dy, i)
                                return False
        # Check if all went well
        # TODO: Testing code. Consider removal.
        for patch in self.patches:
            if None in patch.loc:
                if debug:
                    for patch in self.patches:
                        print(patch.cogRot, patch.loc)
                print('Failed _createGrid2 Loc is None')
                return False
                    
        # Get the bounding box
        boxBB, boxSize=self.getBoundingBox()
        # Store the distance from the CoG, so we keep it consistent 
        # TODO: make magic number configurabale
        self.border=np.max(boxSize)*0.2
        
        # get offset to shift the node position 
        offset=boxBB[0]-[self.border, self.border]
        areaMean=self._calcMeanPatchArea()
        
        #maxDist=*math.sqrt(areaMean)
        self.hiddenPatches=[]
        
        noPatches=[self.config.noPatchesX, self.config.noPatchesY]
        
        # TODO: Inefficient code. Improve.
        for i, x in enumerate(np.linspace(boxBB[0][0], boxBB[1][0], noPatches[0])):
            for j, y in enumerate(np.linspace(boxBB[0][1], boxBB[1][1], noPatches[1])):
                
                node=(x,y)
                #print(node)
                for patch in self.patches:
                    if tuple(patch.loc) == (i, j):
                        patch.cogNode=node-offset
                        break
                else:
                    patch=ColourReferenceElement(node-offset, areaMean, self.config)
                    patch.cogNode=node-offset
                    patch.loc=[i,j]
                    self.hiddenPatches.append(patch)
        # Check if the grid creation was successful
        for patch in self.patches:
            if patch.cogNode is None:
                return False
                
        else:
            return True            
    
    def _createGrid(self, noPatches, tolerance):
        '''
        Creates a rectangular grid and assigns a grid index to all patches within tolerance distance from a grid node
        Grid nodes with no patch in reach are assigned dummy patches.
        
        TODO: This is dead code, consider removing.
        
        :param noPatches: tuple, 2 elements. Number of patches in x and y direction
        :param tolerance: max distance for attaching a patch to a node.
        '''
        # Get the bounding box
        boxBB, boxSize=self.getBoundingBox()
        # Store the distance from the CoG, so we keep it consistent 
        self.border=np.max(boxSize)*0.2
        
        # get offset to shift the node position 
        offset=boxBB[0]-[self.border, self.border]
        areaMean=self._calcMeanPatchArea()
        
        maxDist=tolerance*math.sqrt(areaMean)
        self.hiddenPatches=[]
        
        for i, x in enumerate(np.linspace(boxBB[0][0], boxBB[1][0], noPatches[0])):
            for j, y in enumerate(np.linspace(boxBB[0][1], boxBB[1][1], noPatches[1])):
                
                node=(x,y)
                #print(node)
                for patch in self.patches:
                    dist=distance(patch.cogRot, node)
                    if dist < maxDist: 
                        patch.cogNode=node-offset
                        break
                else:
                    patch=ColourReferenceElement(node-offset, areaMean, self.config)
                    patch.cogNode=node-offset
                    self.hiddenPatches.append(patch)
        # Check if the grid creation was successful
        for patch in self.patches:
            if patch.cogNode is None:
                return False
                
        else:
            return True
        
    def findRemainingPatches(self, rotate=True, debug=False):
        '''
        Tries to find missing patches in the given color checker and reference.i
        It will then compate the colors of the image under test against the reference.
        If the images match sufficiently, the following is returned:
        - A list of patches with corrected positions (can be used for measuring the color of patches.
        - Geometric transformation data. This can be used to transform the image in such a way that it matches a rectangualar geomety.
    
    
        :param rotate: Optional. Defaults to True. If True, rotatePatches is called to find the best roation. If you have already rotated the Patches, set this to False. 
        
        :returns: True if successfull, False if not.
        '''
        
        # Attach the color to each patch and find the brightest patc. 
        # This is used later to compare the color checker to the reference.
        # 
        # From the brightes patch 
        # TODO: Make this more efficient!  

        ## 1. Rotate around the approximate center of patches and perform trapazoid correction
  
        # Rotate the patches using the initial guess so we get a meaningful bounding box.
        if rotate:
            self.rotatePatches()
    
        # Create a grid on which we expect the cog of our patches. Find the closest patch if one exists. 
        # If this does'n work out we fail (=return False)
        # TODO: Condider handling failures in a better way (i.e. other method?)
        if True:
            if not self._createGrid2():
                return False
        else:
            if not self._createGrid([self.config.noPatchesX, self.config.noPatchesY], self.config.toleranceGrid):
                return False
        
    
        # Create a more refined geometric model of the geometry of the color reference
        # We use opencv's perspective transform so we can later on apply the resulting matrix to our image
        #cogsNode=np.array([patch.nodeLocation for patch in self.patches], np.float32)
        cogsReal=np.array([patch.cog for patch in self.patches], np.float32)
        
        # Calculate perspective transforms on image
        self.mForward=self.calcPerspectiveTransform(1, 'forward')
        self.mForwardLarge=self.calcPerspectiveTransform(self.image.ratio, 'forward')
        self.mBackward=self.calcPerspectiveTransform(1, 'backward')
        
        # Calculate the center of gravity for the patches previously not found.
        cogsNode=np.array([patch.cogNode for patch in self.hiddenPatches], np.float32)
        cogsNode=np.array([cogsNode], np.float32)
        cogs=cv2.perspectiveTransform(cogsNode, self.mBackward)[0]
        for cog, patch in zip(cogs, self.hiddenPatches):
            patch.cog=cog
        # Transform the image using the computed transformation matrix
       
        if debug:
            #
            imgTrans=self.imageTransformed(8, 'srgb_brg', True)
            img= self.image.drawCircles(imgTrans, self.config.radiusAvgColor, self.patches, (255, 255, 255), small=True)    
            img= self.image.drawCircles(img, self.config.radiusAvgColor, self.hiddenPatches, (100, 255, 100), small=True)    
            showImage(img)
        
        # Locate the still missing patches and get their color.     
        return True
    
    @property
    def imageSizeResampled(self):
        '''
        Size of the image after perspective transform. The size refers to the small image.
        '''
        cogsNode=np.array([patch.cogNode for patch in self.patches], np.float32)
        
        # Calculate size of image for resampling
        rows,cols=np.max(cogsNode, axis=0) +[self.border, self.border]
        rows,cols=np.array([rows,cols], dtype=np.int)
        size=(rows,cols)
        return size 
    
    def _getDarkestBrigtest(self, debug=False):
        DEBUG=debug
        #img=self.image.getImage(bps=16, linear=True, small=False, brg=False)
        brightest=self.patches[0]
        darkest=self.patches[0]
        patches=self.patches +self.hiddenPatches
        for i, patch in enumerate(patches):
            color = self.image.detectColor(patch.radius, patch.cog, bps=16, color='srgb_brg', small=True)
            patch.color=color
            
            if np.mean(color) > np.mean(brightest.color):
                brightest=patch
            
            if np.mean(color) < np.mean(darkest.color):
                darkest=patch
            
        if DEBUG:    
            patches.remove(darkest)
            patches.remove(brightest)
            patchesGreen=[darkest, brightest]
            #img=self.imageTransformed(16, 'brg', True)
            img=self.image.getImage(16, 'srgb_brg', True)
            img= self.image.drawCircles(img, self.config.radiusAvgColor, patches, (255*100, 255*100, 255*100), small=True)    
            img= self.image.drawCircles(img, self.config.radiusAvgColor, patchesGreen, (100, 255*100, 100), small=True)    
            showImage(img)
            
        return brightest, darkest
    
    def getColor(self, patch, colorModel, image=None, transformed=False):
        '''
        
        :param patch: Instance of :py:class:`crfinder.colorReference.ColorReferenceElement`
        :param colorModel:  Valid options are lab and raw_brg
        :type colorModel: str
        :param image: Optional, if give this image will be used.
        
        :returns: For lab values for channel l, a and b from 0 to 100 and -128 to +128. For rawbrg, channels b, r, g from 0 to 1.
        '''
        
        if transformed:
            cog=np.array(patch.cogNode, dtype=np.int)
        else:
            cog=patch.cog
        DEBUG=False
        if colorModel=='lab':
            xyz = self.image.detectColor(patch.radius, cog, 16, 'xyz', True, DEBUG, image)
            xyz/=2**16
            color = convert_color(XYZColor(*xyz, illuminant='d65'), LabColor)
        elif colorModel == 'raw_brg':
            color= self.image.detectColor(patch.radius, cog, 16, 'raw_brg', True, DEBUG, image)
            color/=2**16
        return(color)
    
    def imageTransformed(self, bps, color, small, fileName=None):
        '''
        Returns the rotated and trimmed image. Optionally writes the image to the disk.
        
        If mForward is not populated, findRemainingPatches will be called to get a perspective transform matrix. 
        
        :param bps: Forwareded to function :py:meth:`crfinder.image.Image.getTransformedImage`.  
        :param color: Forwareded to function :py:meth:`crfinder.image.Image.getTransformedImage`.
        :param small: The choice of the transdormation matrix is based on this. It if further forwarded to Forwareded to function :py:meth:`crfinder.image.Image.getTransformedImage`..
        :param fileName: If FalseFalseFalsegiven, the file will be saved under the given file name.
        
        :returns: None in case of failure. Otherwise, the image is returned as numpy array. 
        '''
        
        if self.mForward is None:
            if not self.findRemainingPatches():
                # If finding of the transformation matrix fails, we cannot return any image :(.
                return None
            
        # Select the correct transformation matrix for the image size
        if small:
            m=self.mForward
        else:
            m=self.mForwardLarge
            
        # The size is independent on the chosen size, as it will be scaled by Image.etTransformedImage
        size=self.imageSizeResampled
        image=self.image.getTransformedImage(size, m, bps, color, small)
        
        # If a filename is given, try to save the file.
        if not fileName is None:
            # opencv requires the color to be brg. 
            # We fail if the color is not on the white list of supported color models. 
            if color=='raw_brg' or color=='srgb_brg':
                cv2.imwrite(fileName, image)
            else:
                raise(RuntimeError('Failed to save image. Unsupported color model.'))
        
        return image
    
    def writeAdjustedImage(self, fileName, targetDark, targetBright, debug=False): 
        '''
        Takes the  transformed image (supplied by imageTransformed) and normalizes the brighness, so the rgb valus for the darkest patch correspond to 
        targetDark and the rgb values for the brightest patch correspond to targetBrigt. Writes the resulting image to disk.
        
        :param fileName:
        :param targetDark: Target rgb value for darkest patch. Supply a value between 0 and 1.
        :type targetDark: float
        :param targetBright: Target rgb value for brightest patch. Supply a value between 0 and 1.
        :type targetBright: float
        
        '''
        
        # We want a reprpoducible orientation of the color checker. 
        # Therefore, find the brightest patch and rotate the color checker to its on the bottom.
        brightest, darkest=self._getDarkestBrigtest()
        if brightest.loc[1]>self.config.noPatchesY/2:
            self._rotate(self.center, self.angle+math.pi)
            # After rotation we have to rebuild the node locations, etc. so we call findRemainingPatches again.
            # This is not nice, as it is computationally expensive, but the easiest thing to update thing
            
            # Reset everthing that is set by findRemainingPatches
            for patch in self.patches:
                 
                patch.initCogNode()
                patch.initLoc()
                patch.initNeighSorted()
            # Call findRemainingPatches to refill patches information.
            self.findRemainingPatches(rotate=False)
        
        DEBUG=debug

        # Save the patches, we need them for fitting the scaling factor and offset.
        self.brightest, self.darkest = self._getDarkestBrigtest()
        
        # Get the size of a square. We don't know the orientation of the image.
        # Therefore, the squre is the inner square of a circle
        radius=brightest.radius/math.sqrt(2)
        size=np.array([radius, radius])
        print('Size for white balance', size)
        # Adjust the white balance
        self.image.performWhiteBalance(brightest.cog, size, small=True)
        
        #xyzBright = self.image.detectColor(brightest.radius, brightest.cog, 16, 'xyz', True)
        
        #print(xyzDark, xyzBright)
        
        #rgbDark = self.getColor(darkest, 'raw_brg') 
        #rgbBright = self.getColor(brightest, 'raw_brg')
        

        # k*(rgbBright + o) = targetBright
        # k*(rgbDark + o) = targetDark
        
        # k*rgbBright + k*o = targetBrigh=
        
        # k=targetBright/(rgbBright + o)
        
        # k=(targetBright -o)/rgbBright
        # o=targetDark-k*rgbDark
        # k=(targetBright -targetDark-k*rgbDark)/rgbBright
        # k=(targetBright -targetDark)/rgbBright- k*rgbDark/rgbBright
        # k + k*rgbDark/rgbBright=(targetBright -targetDark)/rgbBright
        # k(1+rgbDark/rgbBright)=(targetBright -targetDark)/rgbBright
        # k =(targetBright -targetDark)/rgbBright * 1/(1+rgbDark/rgbBright)
        # k =(targetBright -targetDark)/rgbBright * rgbBright/(rgbBright+rgbDark)
        #k = (targetBright -targetDark)/rgbBright * rgbBright/(rgbBright+rgbDark)
        ##o = targetDark-k*rgbDark
        #oF16Bit=np.mean(o*2**6)
        
        # Initial guess has  a minima offset and almost no scaling
        initialGuess=(1, 2.1, 2.2, 2.3)
        
        # get the offset by fitting. Yes, this is very inefficient. 
        # TODO: Implement for LAB and not raw rgb. 
        # TODO: Consider making this more efficient 
        param, _=leastsq(self._errorColor, initialGuess, args=(targetBright, targetDark), epsfcn=1e-2)
        param, _=leastsq(self._errorColor, param, args=(targetBright, targetDark))
        
        offset=param[0]
        factor=param[1:4]
        
        # Get the untransformed image so we can apply getColor
        imageAdapted=self._scaleColor(offset, factor, True, transformed=False)
        rgbDark = self.getColor(darkest, 'raw_brg', imageAdapted, transformed=False) 
        rgbBright = self.getColor(brightest, 'raw_brg', imageAdapted, transformed=False)
        #image
        print('Scaling factor: {}, Offset: {}'.format(factor, offset))
        print('RGB darkest after sclaing: ', rgbDark, 'Target avg.:', 'b')
        print('RGB brightest after scaling: ', rgbBright, 'Target avg.:', 'b')
        
        # Get the transformed image for output
        imageAdapted=self._scaleColor(offset, factor, transformed=True)
        #print(np.min(imageAdapted))
        #print(np.max(imageAdapted))
        #self.image.setBlackWhite()
        if DEBUG:
            #imgSmall=self.image.getImage(8, 'brg', True)
      #      cv2.imshow("Image", image)
       #     cv2.waitKey(0)
            showImage(imageAdapted)
        
        writer=tifffile.TiffWriter(fileName)
        writer.save(imageAdapted[:,:,::-1], photometric='rgb')
        #tiff = TIFF.open(fileName, mode='w')
        
        #tiff.write_image(imageAdapted[:,:,::-1])
        #tiff.close()
        #cv2.imwrite(fileName, imageAdapted)
        
        # Now we have brighteFrom that we can estimate t
        
        # We assume the brightest patch was correctly detected. 
        # TODO: This is not true for references with white background. Implement this 
        # for such referneces too. 
        #for patch in self.hiddenPatches:
        #   patch.detectColor(imgTrans, config.RADIUSAVGCOLOR, True)
        
    def _errorColor(self, param, targetBright, targetDark):
        '''
        Calcuales the error between the color of the darkest and brightest patch and the target values
        
        
        
        '''
        
        o=param[0]
        k=param[1:4]
        #k=param#[1:4]
        #o=0
        image=self._scaleColor(o, k, True, transformed=False)
        tst=np.mean(image)
        #print(tst)
    
        rgbDark = self.getColor(self.darkest, 'raw_brg', image, transformed=False) 
        rgbBright = self.getColor(self.brightest, 'raw_brg', image, transformed=False)
        
#        print(rgbBright, targetBright)
        error= np.subtract(rgbBright, targetBright)
        error=np.hstack((error, np.mean(np.subtract(rgbDark, targetDark))))
        result=np.array(error).flatten()
        print(param, result)
        if np.any(k):
            result*=1E12 
        return result
    
    def _scaleColor(self, offset, factor, floatOut=False, transformed=True, debug=False):
        '''
        Scales the color of the image according to 
        colorNew=k*(colorOld+o) where k is the parameter factor and o is the parameter offset.
        
        :param offset: Offset between 0 and 1E4. 1E4 corresponds to full white. 0 to black
        :param factor: scaling factor. Can be float or 3 values numpy array for b, r, g
        
        :returns: Scales variant of the image, 16 bit, raw_brg 
        '''
        
        oF16Bit=np.mean((offset/1E4)*2**16)
        if transformed:
            image=self.imageTransformed(16, 'raw_brg', False)
        else:
            image=self.image.getImage(16, 'raw_brg', True)
        #tmp=np.subtract(image, oF16Bit)
        #imageAdapted=np.multiply(tmp, factor)
        tmp=np.multiply(image, factor)
        imageAdapted=np.subtract(tmp, oF16Bit)
        
        # + o
        
        if not floatOut:
            np.clip(imageAdapted, 0, 2**16-1, out=imageAdapted)
            imageAdapted=imageAdapted.astype(np.uint16)
        
        if debug:
            imageDebug=imageAdapted.astype(np.uint16)
            showImage(imageDebug)
        return imageAdapted