# -*- coding: utf-8 -*-


# Copyright 2018, Simon Waid
# This file is part of CrFinder.
#
#    CrFinder is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    CrFinder is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#
#    Diese Datei ist Teil von CrFinder.
#
#    CrFinder ist Freie Software: Sie können es unter den Bedingungen
#    der GNU General Public License, wie von der Free Software Foundation,
#    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
#    veröffentlichten Version, weiter verteilen und/oder modifizieren.
#
#    CrFinder wird in der Hoffnung, dass es nützlich sein wird, aber
#    OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
#    Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
#    Siehe die GNU General Public License für weitere Details.
#
#    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
#    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.


import numpy as np
import cv2
from matplotlib import pyplot as plt
import igraph
import copy
from crfinder.tools import distance, showImage
from crfinder.colorRef import ColourReferenceElement




def mergeByNeighborhood(image, candidates, config, debug):
    '''
    Merges potential color checker patches by their geometric position to potential 
    color checkers. Lonely patches are removed
    
    :param image: image in brg color space
    :param candiates: list of instances of ColorCheckerElement as returned by findCandiatePatches
    :param config: Instance of Configuration
    
    :returns colorCheckers: returns a list of lists of patches. Each element in the first list is a potential color checker
    
    '''
    DEBUG=debug
    
    print('We have {} candidate patches. Now bundling to color checkers'.format(len(candidates)))
    print('Using {} bins. Tolerance is {}'.format(config.NoBins, config.toleranceSize))
    # Maximum area of a patch is the image area divided by the number of patches  
    areaImage=image.shape[0]*image.shape[1]
    # We sort by sideleght. We approximate it as the square root of the area.
    slBin=np.sqrt(areaImage)/config.NoBins
    # Create the bins
    bins=[]
    for i in range(config.NoBins):
        bins.append([])#[i*slBin]=[]
    
    # Attach the candidates to the bins.
    for candidate in candidates:
        # Calculate the sidelenght
        slCanidate=np.sqrt(candidate.area)
        # We have some tolerance on the sidelenght when butting the candidate into a bin.
        slMin=slCanidate*(1-config.toleranceSize)
        slMax=slCanidate*(1+config.toleranceSize)
        # The bin indices can be calculated from the relation between sidelanght to the bin sidelenght.
        idxStart=int(slMin/slBin)
        # We want to have each candiate at least in two bins so add 1.
        idxEnd=int(slMax/slBin)+1
        # Add candidates to bins
        for i in range(idxStart, idxEnd):
            bins[i].append(candidate)
    
    
    # We restrict ourselfes o candidates which have an sufficient number of neighbours.
    # This will be the case for the color checker as patches are adjacent to each other
    # Loop over each bin
    binCandidates=[]
    for idxBin, thisBin in enumerate(bins):
        # If there is an insufficient number of candidates in the bin, 
        # delete the bin and skip it..
        if len(thisBin) < config.minPatches or  len(thisBin) > config.maxPatches:
            thisBin=[]
            continue
        
       
        # Create a graph so we can later on search for the neighbors of patches which
        # will actually describe the color checker.
        graph = igraph.Graph()
        graph.add_vertices(len(thisBin))
        
        # This is comutationally expensinve. 
        # We could sort by x and y axis and only search for neighbours.
        # Alternative methods might also work out (i.e. use hirarchy of findContours
        # Alternatively we might also want to make some lookup where we can index candidates
        # by their location
        for i, candidateA in enumerate(thisBin):
            # We expect a neighbor to be at a maxium distance of MAXNEIGHBOURDIST
            # MAXNEIGHBOURDIST is reative to the nominal bin sidelenght
            
            maxDist1=idxBin*slBin*config.maxNeighbourDist
            #maxDist2=candidateA.sideLenght*config.maxNeighbourDist
            #maxDist=min(maxDist1, maxDist2)
            maxDist=maxDist1
            #print(maxDist)
            for j, candidateB in enumerate(thisBin[i:]):
                j=j+i
                # Check if there is a neighbour in the allowed distance. If so, 
                # append edges to the graph. 
                if i != j:
                    cogA=candidateA.cog
                    cogB=candidateB.cog
                    dist =distance(cogA, cogB )
                    if dist < maxDist:
                        #print('dist', dist, cogA, cogB , i, j)
                        graph.add_edges([(i, j)])
                        candidateA.addNeighbour(candidateB)       
                        candidateB.addNeighbour(candidateA)
        
        # Let's decompose the graph into blocks (we could call neighbourhouds):
        graphBlocks=graph.blocks()
        for graphDec in graphBlocks:
            # Check how many elements the block has. Ignore small blocks
            if len(graphDec) > config.minPatches:
                if DEBUG:
                    imgForDebug=copy.deepcopy(image)
                print('Bin no {} has a block of {} patches. Of those {} are neighbours'.format(idxBin, graph.vcount(), len(graphDec)))
                #print(graph.vs.indices)
                #print(graphDec)

                for idx in graph.vs.indices:
                    patch=thisBin[idx]
                    cX, cY=patch.cog
                    if DEBUG:
                        cv2.putText(imgForDebug, 'P', (cX-10, cY), cv2.FONT_HERSHEY_SIMPLEX,
                            0.5, (255, 255, 255), 2)
                
                #  Attach all patches in this block to a list
                thisBlock=[]
                for idx in graphDec:#.vs.indices:
                    patch=thisBin[idx]
                    thisBlock.append(patch)
                    cX, cY=patch.cog
                    if DEBUG:
                        cv2.putText(imgForDebug, 'C', (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
                            0.5, (255, 255, 255), 2)
 
                # Attach this block to the list of remaining candidates
                binCandidates.append(thisBlock)
                # show the output image
                if DEBUG:
                    showImage(imgForDebug)
    
    print('We have {} potential color checkers. Now removing duplicates.'.format(len(binCandidates)))

    # Now we have some potential color checkers. Some might contain the same 
    # patches (as they have been detected in different bins). Merge them together.
    # TODO:  Consider finding a more efficient way.
    #  
    for i, candidateA in enumerate(binCandidates):
        for j, candidateB in enumerate(binCandidates[i:]):
            j=j+i
            # Check if there is a neighbour in the allowed distance. If so, 
            # append edges to the graph. 
            if i != j:
                # Get common elements between the two c.astype(np.float)anidats.
                intersection=list(set(candidateA).intersection(candidateB))
                if not intersection == []:
                    # If common elements are present, mark the candiate with the lower number of elements for deletion.
                    if len(candidateA) > len(candidateB):
                        binCandidates[j]=[]
                    else:
                        binCandidates[i]=[]
    # Remove candidates marked for deletion
    candiates=[]
    for candidate in binCandidates:
        if candidate != []:
            candiates.append(candidate)
    
    print('After the removal of duplicates {} protential color checkers are left'.format(len(candiates)))
    
    return candiates


def findCandiatePatches(image, config, threshold, debug, debugFile=None):
    '''
    Searches the given image for patches potetially belonging to a color checker
    
    :param iamge: open cv compatible image in brg color space
    '''
    DEBUG=debug
    # Normalize the image
    #normalizedImg = np.zeros_like(image)
    #normalizedImg = cv2.normalize(image,  normalizedImg, 0, 255, cv2.NORM)
   
    # Resizing is now done in the Image class. The resize ratio is therefore 1.
    #ratio = image.shape[0] / float(imgResized.shape[0])
    ratio=1.
    
    imgHsv=cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    #channelH=imgHsv[:,:,0]
    #channelS=imgHsv[:,:,1]
    channelV=imgHsv[:,:,2]
    #plt.imshow(channelh, 'gray')#,plt.show()
    
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    img = clahe.apply(channelV)
    # Blur the image to reduce noise. This improves detection for high-iso or badly lit iages
    # TODO: Make this dependent on the actual noise in the image.
    # TODO: Consder bluring the image before resizing it.
    blurred = cv2.GaussianBlur(img, (5, 5), 0)
    
    if debug:
        plt.imshow(blurred, 'gray'),plt.show()
    
    img = cv2.threshold(blurred, threshold, 255, cv2.THRESH_BINARY)[1]
    if DEBUG:
        plt.imshow(img, 'gray'),plt.show()
    
    #adapt= cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
        
    ref=0.1*255

    lower = int(max(0, ref))
    upper = int(min(255, ref))
                
    canny = cv2.Canny(img, lower, upper,apertureSize = 3)
 
    if DEBUG:
        plt.imshow(canny, 'gray')#,plt.show()
   
    im2, contours, hierarchy= cv2.findContours(canny.copy(), cv2.RETR_TREE,
                            cv2.CHAIN_APPROX_SIMPLE)
    
    # If finding contours failed fail
    if hierarchy is None:
        return []
    
    candidates=[]

    # loop over the contours
    if DEBUG:
        imgForDebug=copy.deepcopy(image)
    for c, h in zip(contours, hierarchy[0]):
        # compute the center of the contour, then detect the name of the
        # shape using only the contour
        c=c[:,0,:]
        M = cv2.moments(c)
        if M["m00"]==0.0:
            continue
        cX = int((M["m10"] / M["m00"]) * ratio)
        cY = int((M["m01"] / M["m00"]) * ratio)
                       
        # Store center of gravity
        cog=[cX, cY]
        
        # Store area
        area=M['m00']
        
        # multiply the contour (x, y)-coordinates by the resize ratio,
        # then draw the contours and the name of the shape on the image
        c = c.astype("float")
        c *= ratio
        c = c.astype("int")
        if DEBUG:
            cv2.drawContours(imgForDebug, [c], -1, (0, 255, 0), 2)
        #cv2.circle(img1, (cX, cY), 7, (255, 255, 255), -1)
        #cv2.putText(brg, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
        #0.5, (255, 255, 255), 2)
 
        # We are looking for square patches.
        
        #Get the aspect ratio so we can check if the patch is square
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.04 * peri, True)
        (x, y, width, height) = cv2.boundingRect(approx)
        edges=len(approx)
        aspect = width/ float(height)
        #print(x, y, edges, aspect)
        
        # TODO: make this configurable
        # TODO: the bounding rectangle is not very precise, as the patches can be rotated. 
        # TODO: Check the positions of the edges instead and cacualte the aspect ratio from that
        if edges ==4 and  aspect >= 0.7 and aspect <= 1.6: 
            # Also the patches don't have any children.
            # Therefore the child entry in hirarchy is negative
            if h[2] <0:
                candidates.append(ColourReferenceElement(cog, area, config))
                if DEBUG:
                    cv2.putText(imgForDebug, 'CANIDATE', (cX, cY), cv2.FONT_HERSHEY_PLAIN,
                            0.5, (255, 255, 255), 2)
        else:
            if DEBUG:
                cv2.putText(imgForDebug, '{} {:2.1f}'.format(edges, aspect), (cX, cY), cv2.FONT_HERSHEY_PLAIN, 0.5, (255, 255, 255), 2)
            
 
        # show the output image
    if DEBUG:
        showImage(imgForDebug)
    return candidates